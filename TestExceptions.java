public class TestExceptions {
    public static void main(String[] args) {
        Account[] arrayOfAccounts = new Account[3];
        String[] names = {"Fingers", "Tim", "tom"};
        double[] amount = {25, 100, 2400};
        try {
            for (int i=0; i<arrayOfAccounts.length; i++){
                arrayOfAccounts[i].setName(names[i]);
                arrayOfAccounts[i].setBalance(amount[i]);

                System.out.println("The name is " + arrayOfAccounts[i].getName());
                System.out.println("The balance is "+ arrayOfAccounts[i].getBalance());
                arrayOfAccounts[i].addInterest();
                System.out.println("The balance is now "+ arrayOfAccounts[i].getBalance());;
            }
        } 
        catch (DodgyNameException e) {
            System.out.println("Exception:" + e);
            //return;

        }
        // finally {
        //     double taxTotal = 0;
        //     for (int i = 0; i< arrayOfAccounts.length; i++) {
        //         taxTotal += arrayOfAccounts[i].getBalance()*0.4;
        //         arrayOfAccounts[i].setBalance(arrayOfAccounts[i].getBalance()*0.6);
        //     }
        //     System.out.println("Total tax collected is "+ taxTotal);

        // }
    
    }
}