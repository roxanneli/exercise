

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class TestStrings {
    public static void main(String[] args) {
        StringBuilder myString = new StringBuilder("example.doc");
        myString.replace(myString.length()-3,myString.length(),"bak");
        System.out.println(myString);

        String name1 = "peter";
        String name2 = "tom";
        if (name1.equals(name2)) {
            System.out.println("same strings");
        }

        String y = (name1.compareTo(name2) > 0) ? name1:name2;
        System.out.println(y + " is the greater string.");

        String text = "the quick brown fox swallowed down the lazy chicken";
        int count = 0;
        for (int i = 0; i< text.length()-2; i++) {
            if (text.substring(i,i+2).equals("ow")) {
                count++;
            }
        }
        System.out.println("The occurance is " + count);

        StringBuilder str1 = new StringBuilder("Live not on evil");
        StringBuilder str2 = new StringBuilder(str1);
        if (str1.reverse().toString().equalsIgnoreCase(str2.toString())) {
            System.out.println("This is palindrome");
        }

        Format yearOnly = new SimpleDateFormat("yyyy");
        Format dayMonthYear = new SimpleDateFormat("dd MM yyyy");
        Format time = new SimpleDateFormat("hh mm");
        System.out.println(yearOnly.format(new Date()));
        System.out.println(dayMonthYear.format(new Date()));
        System.out.println(time.format(new Date()));

    }

}