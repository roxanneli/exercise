
public class TestAccount2 {
    public static void main(String[] args) {
        
    
        Account[] arrayOfAccounts = new Account[5];
        double[] amounts = {23,333,322,323,222};
        String[] names = {"peter", "tom", "tim", "william", "mary"};

        for (int i=0;i<arrayOfAccounts.length;i++) {
            arrayOfAccounts[i] = new Account(names[i],amounts[i]);
            System.out.println("The account name is " + arrayOfAccounts[i].getName());
            System.out.println("The account balance is " +arrayOfAccounts[i].getBalance());
            arrayOfAccounts[i].addInterest();
            System.out.println("The new account balance is " + arrayOfAccounts[i].getBalance());

        }   
        arrayOfAccounts[0].withdraw(30);
        arrayOfAccounts[1].withdraw();
        arrayOfAccounts[2].withdraw(100);
    }
    
}