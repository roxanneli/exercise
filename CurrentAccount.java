public class CurrentAccount extends Account {

    public CurrentAccount(String name, double balance) {
        super(name, balance);
    }

    //subclass currentAccount override the abstract methods
    @Override
    public void addInterest() {
        setBalance(getBalance()*1.1);
    }
    
}