import java.util.Comparator;

//sort Collections
public class AccountComparator implements Comparator<Account>{

    @Override
    public int compare(Account a1, Account a2) {
        // TODO Auto-generated method stub
        if (a1.getBalance() < a2.getBalance()) return -1;
        else if (a1.getBalance() > a2.getBalance()) return 1;
        else return 0;
    }

    //? TreeSet<Account>((a1, a2) -> compare(a1, a2));
    
}
