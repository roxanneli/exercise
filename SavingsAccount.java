public class SavingsAccount extends Account {

    public SavingsAccount(final String name, final double balance) {
        super(name, balance);
    }
    @Override
    public void addInterest() {
        setBalance(getBalance()*1.4);
    }

}