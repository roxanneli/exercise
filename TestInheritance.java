public class TestInheritance {
    public static void main(String[] args) {
        Account[] accounts = { new Account("tom",2), 
            new SavingsAccount("peter", 4), new CurrentAccount("tim", 6)};
        
        for (int i = 0; i<accounts.length; i++){
            accounts[i].addInterest();
            System.out.println("Name " + accounts[i].getName());
            System.out.println("Balance " + accounts[i].getBalance());
        }

    }
}