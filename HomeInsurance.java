public class HomeInsurance implements Detailable{
    private final double premium;
    private final double excess;
    private final double amountInsured;

	public HomeInsurance(final double premium, final double excess, final double amountInsured) {
		this.premium = premium;
		this.excess = excess;
		this.amountInsured = amountInsured;
	}

    @Override
    public String getDetails() {
        // TODO Auto-generated method stub
        return "Information about the policy: premium is " 
        +premium + ", excess is " + excess + 
        ", amount insured is " + amountInsured;
        


    }
    
}