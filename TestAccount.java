public class TestAccount {
    public static void main(String[] args) {
        // Account myAccount = new Account();
        // myAccount.setName("saving");
        // myAccount.setBalance(2000);

        // System.out.println("The account name is "+ myAccount.getName());
        // System.out.println("The account balance is "+ myAccount.getBalance());
        // myAccount.addInterest();
        // System.out.println("The new account balance is " + myAccount.getBalance());

        Account[] arrayOfAccounts = new Account[5];
        double[] amounts = {23,333,322,323,222};
        String[] names = {"peter", "tom", "tim", "william", "mary"};

        for (int i=0;i<arrayOfAccounts.length;i++) {
            arrayOfAccounts[i] = new Account();
            arrayOfAccounts[i].setName(names[i]);
            arrayOfAccounts[i].setBalance(amounts[i]);
            System.out.println("The account name is " + arrayOfAccounts[i].getName());
            System.out.println("The account balance is " +arrayOfAccounts[i].getBalance());
            arrayOfAccounts[i].addInterest();
            System.out.println("The new account balance is " + arrayOfAccounts[i].getBalance());

        }



        
    }
    
}