import java.util.*;
public class CollectionsTest{
    //public Account(String accountName, double balance){};
    public static void main(String[] args) {
        //part 1
        Set<Account> accounts = new HashSet<Account>();
        //part 2
        //Set<Account> accounts = new TreeSet<Account>();
        
        Account a1 = new Account();
        Account a2 = new Account("peter",3000.3);
        Account a3 = new Account("mary", 4.5);
        a1.setAccountName("mary"); a1.setBalance(5);
        a2.setAccountName("PETER"); a2.setBalance(3000);
        a3.setAccountName("hans"); a3.setBalance(525);
        /* for (int i=0; i<5;i++){
            current = new Account();
            current.setAccountName(names[i]);
            current.setBalance(amounts[i]);
            accounts.add(current);

        }  */
        
        accounts.add(a1);
        accounts.add(a2);
        accounts.add(a3);
        //use the iterator
        Iterator<Account> iter = accounts.iterator();
        while (iter.hasNext()){
            Account a = iter.next();
            System.out.println("The account name is "+ a.getAccountName() + ", the balance is "+ a.getBalance());
        }

        //use Java5 for each
        for (Account current:accounts){
            System.out.println("The account name is "+ current.getAccountName() + ", the balance is "+ current.getBalance());
        }

        //use Java8 forEach
        accounts.forEach(a -> System.out.println("The account name is "+ a.getAccountName() + ", the balance is "+ a.getBalance()));

        
    }
}