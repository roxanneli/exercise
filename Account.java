public class Account {
    //properties
    private double balance;
    private String name;
    private static double interstRate = 0.2;



    public Account(String name, double balance) throws DodgyNameException{
        if ("Fingers".equals(name)){
            throw new DodgyNameException();
        }
        this.name = name;
        this.balance = balance;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) throws DodgyNameException {
        if ("Fingers".equals(name)){
            throw new DodgyNameException();
        }
        this.name = name;
    }

    public void addInterest() {
        //balance =  balance + balance*interstRate;
    }

    public Account( String name) throws DodgyNameException {
        if ("Fingers".equals(name)){
            throw new DodgyNameException();
        }
        this.name = name;
    }

    public static double getInterstRate() {
        return interstRate;
    }

    // public static void setInterstRate(double interstRate) {
    //     Account.interstRate = interstRate;
    // }

    public boolean withdraw (){
        return withdraw(20);
    }

    public boolean withdraw(double amount) {
        boolean flag = false;
        if (balance > amount){
            flag = true;
            balance -= amount;
            System.out.println("There is enough money to withdraw. You withdraw " + amount);
            System.out.println("The balance is now " + balance );
        }
        else {
            System.out.println("There is not enough money to withdraw. The balance is " + balance);
        }
        return flag;

    }



}